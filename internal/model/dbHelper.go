package model

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/drmule100/trivia/internal/common"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	defaultScheme    = "mongodb+srv:"
	defaultDBTimeout = 30
)

// LookupTrivia
func LookupTrivia() {
	fmt.Println("fetching trivia from DB")
}

// Connect creates the mongo client to establish connection to mong db
func Connect(dbName string) (mgo *mongo.Client, db *mongo.Database, err error) {

	var uri string
	env := os.Getenv("ENVIRONMENT")
	if env == "" || strings.EqualFold(env, "local") {
		uri = "mongodb://localhost:27017" // todo check username and pwd
	} else {

		dbHost := os.Getenv("APP_DB_HOST")
		if dbHost == "" {
			// todo add logs
			return mgo, db, errors.New("APP_DB_HOST is not set as env variable")
		}

		dbOpts := os.Getenv("APP_DB_CONNECT_OPTIONS")
		if dbOpts == "" {
			return mgo, db, errors.New("APP_DB_CONNECT_OPTIONS is not set as env variable")
		}

		dbUname := os.Getenv("APP_DB_USERNAME")
		if dbUname == "" {
			fmt.Println("APP_DB_USERNAME env var is not set")
			return mgo, db, errors.New("APP_DB_USERNAME env var is not set")
		}

		dbPassword := os.Getenv("APP_DB_PASSWORD")
		if dbPassword == "" {
			fmt.Println("APP_DB_PASSWORD env var is not set")
			return mgo, db, errors.New("APP_DB_PASSWORD env var is not set")
		}

		var scheme string
		var maskedURI string

		// using default format to form uri
		uri = fmt.Sprintf("%s%s:%s@%s/test?%s", scheme, dbUname, dbPassword, dbHost, dbOpts)
		maskedURI = fmt.Sprintf("%s%s:***@%s/test?%s", scheme, dbUname, dbHost, dbOpts)

		fmt.Println(maskedURI)
	}

	//connect to MongoDB
	mgo, err = mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		fmt.Println(err)
		return mgo, db, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(defaultDBTimeout)*time.Second)
	defer cancel()

	err = mgo.Connect(ctx)
	if err != nil {
		fmt.Println("Error connecting to MongoDB")
		return mgo, db, err
	}

	db = mgo.Database(dbName)

	fmt.Println("Connected to MongoDB:", db.Name())

	return mgo, db, nil
}

func Disconnect(mgo *mongo.Client) error {
	fmt.Println("Disconnecting MongoDB")
	err := mgo.Disconnect(context.TODO())
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (dbctx *Context) getFilterAndSortBsonFromMap(searchBy []common.FilterFieldValue, sortBy map[string]int) (filter bson.M, sort bson.D, err error) {

	//check to see if filter fields are part of the approved list
	found := func(filterField common.FilterFieldValue, list []common.FilterField) common.FilterField {
		for _, v := range list {
			if v.BsonName == filterField.BsonName {
				return v
			}
		}
		return common.FilterField{}
	}

	filter = bson.M{}
	for k, v := range searchBy {
		if v.BsonName == "" {
			return nil, nil, errors.New("empty value specified for filter property")
		}
		ff := found(v, dbctx.collection.AllowedFilter)
		if ff.BsonName == "" {
			return nil, nil, fmt.Errorf("[%v] - invalid field specified for filter", k)
		}

		switch ff.Datatype {
		case common.DataTypeString:
			filter, err = getBsonForString(v, filter)
		case common.DataTypeNumber:
			filter, err = getBsonForNum(v, filter)
		case common.DataTypeDate:
			filter, err = getBsonForDate(v, filter)
		case common.DataTypeBool:
			filter, err = getBsonForBool(v, filter)
		case common.DataTypeInterface:
			filter, err = getBsonForInterface(v, filter)
		default:
			return nil, nil, errors.New("invalid data type for filter")
		}
		if err != nil {
			return nil, nil, err
		}
	}

	if len(sortBy) == 0 {
		return filter, nil, nil
	}

	for k, v := range sortBy {
		sort = append(sort, bson.E{Key: k, Value: v})
	}

	return filter, sort, nil
}

func getBsonForString(ffv common.FilterFieldValue, filter bson.M) (bson.M, error) {
	if ffv.SearchType == "" {
		ffv.SearchType = common.SearchTypePATTERN
	}
	if ffv.SearchType == common.SearchTypePATTERN {
		filter[ffv.BsonName] = primitive.Regex{
			Pattern: "^" + ffv.SearchValue,
			Options: "i",
		}
	}
	if ffv.SearchType == common.SearchTypeEXACT {
		filter[ffv.BsonName] = ffv.SearchValue
	}

	if ffv.SearchType == common.SearchTypeIN {
		filter[ffv.BsonName] = bson.M{"$in": ffv.SearchList}
	}

	if ffv.SearchType == common.SearchTypeNOT_EQUAL {
		filter[ffv.BsonName] = bson.M{"$ne": ffv.SearchValue}
	}
	return filter, nil
}

func getBsonForBool(ffv common.FilterFieldValue, filter bson.M) (bson.M, error) {
	search, err := strconv.ParseBool(ffv.SearchValue)
	if err == nil {
		if ffv.SearchType == common.SearchTypeEXACT || ffv.SearchType == "" {
			filter[ffv.BsonName] = search
		}
	} else {
		return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a boolean", ffv.BsonName, ffv.SearchValue)
	}
	return filter, nil
}

func getBsonForNum(ffv common.FilterFieldValue, filter bson.M) (bson.M, error) {

	if ffv.SearchType == common.SearchTypeEXACT || ffv.SearchType == "" {
		search, err := strconv.ParseFloat(ffv.SearchValue, 64)
		if err == nil {

			filter[ffv.BsonName] = search

		} else {
			return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a number", ffv.BsonName, ffv.SearchValue)
		}
	}
	if ffv.SearchType == common.SearchTypeRANGE {
		if ffv.StartValue != "" && ffv.EndValue != "" {
			searchFrom, errFrom := strconv.ParseFloat(ffv.StartValue, 64)
			if errFrom != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a number", ffv.BsonName, ffv.StartValue)
			}
			searchTo, errTo := strconv.ParseFloat(ffv.EndValue, 64)
			if errTo != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a number", ffv.BsonName, ffv.EndValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$gt": searchFrom, "$lt": searchTo,
			}
		} else if ffv.StartValue != "" {
			searchFrom, errFrom := strconv.ParseFloat(ffv.StartValue, 64)
			if errFrom != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a number", ffv.BsonName, ffv.StartValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$gt": searchFrom,
			}
		} else if ffv.EndValue != "" {
			searchTo, errTo := strconv.ParseFloat(ffv.EndValue, 64)
			if errTo != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a number", ffv.BsonName, ffv.EndValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$lt": searchTo,
			}
		}

	}
	if ffv.SearchType == common.SearchTypeIN {
		var searches = []float64{}
		var errs = []string{}
		for _, v := range ffv.SearchList {
			num, err := strconv.ParseFloat(v, 64)
			if err != nil {
				errs = append(errs, v)
			} else {
				searches = append(searches, num)
			}

		}
		if len(errs) > 0 {
			return nil, fmt.Errorf("[%s] - data type mismatch. [%v]  not a number", ffv.BsonName, errs)
		}
		filter[ffv.BsonName] = bson.M{"$in": searches}
	}
	return filter, nil
}

func getBsonForDate(ffv common.FilterFieldValue, filter bson.M) (bson.M, error) {

	if ffv.SearchType == common.SearchTypeEXACT {
		searchDate, err := time.Parse(time.RFC3339, ffv.SearchValue)
		if err != nil {
			return nil, fmt.Errorf("[%s] - data type mismatch. [%s]  not a valid date", ffv.BsonName, ffv.SearchValue)
		}
		filter[ffv.BsonName] = searchDate
	}

	if ffv.SearchType == common.SearchTypeRANGE {
		if ffv.StartValue != "" && ffv.EndValue != "" {
			searchFrom, errFrom := time.Parse(time.RFC3339, ffv.StartValue)
			if errFrom != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a valid date", ffv.BsonName, ffv.StartValue)
			}
			searchTo, errTo := time.Parse(time.RFC3339, ffv.EndValue)
			if errTo != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a valid date", ffv.BsonName, ffv.EndValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$gt": searchFrom, "$lt": searchTo,
			}
		} else if ffv.StartValue != "" {
			searchFrom, errFrom := time.Parse(time.RFC3339, ffv.StartValue)
			if errFrom != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a valid date", ffv.BsonName, ffv.StartValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$gt": searchFrom,
			}
		} else if ffv.EndValue != "" {
			searchTo, errTo := time.Parse(time.RFC3339, ffv.EndValue)
			if errTo != nil {
				return nil, fmt.Errorf("[%s] - data type mismatch. [%s] is not a valid date", ffv.BsonName, ffv.EndValue)
			}
			filter[ffv.BsonName] = bson.M{
				"$lt": searchTo,
			}
		}

	}
	return filter, nil
}

func getBsonForInterface(ffv common.FilterFieldValue, filter bson.M) (bson.M, error) {
	if _, err := strconv.Atoi(ffv.SearchValue); err != nil {
		return getBsonForString(ffv, filter)
	}
	return getBsonForNum(ffv, filter)
}

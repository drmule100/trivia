package model

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/drmule100/trivia/internal/common"
	trivia "gitlab.com/drmule100/trivia/pkg"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Context struct {
	logger     *logrus.Logger
	db         *mongo.Database
	collection *common.CollRule
}

const (
	CollectionName = "trivia"
)

// filter fields

var defaultIDFilter = common.FilterField{
	BsonName:   "_id",
	StructName: "ID",
	Datatype:   common.DataTypeString,
}

var textFilter = common.FilterField{
	BsonName:   "text",
	StructName: "Text",
	Datatype:   common.DataTypeString,
}

var numberFilter = common.FilterField{
	BsonName:   "number",
	StructName: "Number",
	Datatype:   common.DataTypeInterface,
}

var foundFilter = common.FilterField{
	BsonName:   "found",
	StructName: "Found",
	Datatype:   common.DataTypeBool,
}

var typeFilter = common.FilterField{
	BsonName:   "type",
	StructName: "Type",
	Datatype:   common.DataTypeString,
}

func New(l *logrus.Logger, db *mongo.Database) (*Context, error) {

	collection := &common.CollRule{
		Name:          CollectionName,
		ParentDB:      db,
		AllowedFilter: []common.FilterField{textFilter, numberFilter, typeFilter, foundFilter},
		DefaultID:     defaultIDFilter,
	}
	return &Context{logger: l, collection: collection, db: db}, nil
}

func (dCtx *Context) LoadDocuments(ctx context.Context) error {

	// we are loading documents into db if there are no documents
	collection := dCtx.db.Collection(CollectionName)
	reslt := collection.FindOne(ctx, bson.M{}, &options.FindOneOptions{})
	if reslt.Err() == nil { // no error means document is already present
		return nil
	}

	pwd, err := os.Getwd()
	if err != nil {
		return err
	}
	folder := pwd + string(os.PathSeparator) + "internal" + string(os.PathSeparator) + "json" + string(os.PathSeparator)

	content, err := ioutil.ReadFile(folder + "documents.json")
	if err != nil {
		return err
	}
	var trivias []trivia.Trivia

	err = json.Unmarshal(content, &trivias)
	if err != nil {
		return err
	}
	docs := make([]interface{}, 0)

	for _, v := range trivias {
		if v.Number == nil {
			v.Number = "null"
		}
		docs = append(docs, v)
	}

	_, err = collection.InsertMany(ctx, docs)
	return err
}
func (dbCtx *Context) FetchDocuments(ffvs []common.FilterFieldValue, sortBy map[string]int, skipCnt, limitCnt int64, projection bson.D, resultSlice interface{}, trxId string) error {

	filter, sort, err := dbCtx.getFilterAndSortBsonFromMap(ffvs, sortBy)
	if err != nil {
		dbCtx.logger.Errorf("[%s]: error while validating filter and sort values: %v", trxId)
		return err
	}

	dbCtx.logger.Debugf("[%s]: received filter %v and sort %v for fetch documents", trxId, filter, sort)
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	defer cancel()

	var opts *options.FindOptions

	opts = options.Find().SetMaxTime(30 * time.Second)

	if sort != nil {
		opts = options.Find().SetSort(sort)
	}

	if projection != nil {
		opts = opts.SetProjection(projection)
	}

	if limitCnt > 0 {
		opts = opts.SetLimit(limitCnt)
	}

	if skipCnt > 0 {
		opts = opts.SetSkip(skipCnt)
	}

	dbColl := dbCtx.db.Collection(dbCtx.collection.Name)

	cur, err := dbColl.Find(ctx, filter, opts)

	if err != nil {
		dbCtx.logger.Errorf("[%s]: error while db.find: %v", trxId, err)
		return err
	}
	defer cur.Close(ctx)

	if cur == nil {
		dbCtx.logger.Errorf("[%s]: empty curser from db.", trxId)
		return errors.New("empty curser from db")
	}

	if err = cur.All(ctx, resultSlice); err != nil {
		dbCtx.logger.Errorf("[%s]: error while unmarshiling data into resultslice: %v", trxId, err)
		return err
	}

	return nil
}

package handlers

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/drmule100/trivia/internal/common"
	"gitlab.com/drmule100/trivia/internal/service"
	"gitlab.com/drmule100/trivia/internal/util"
)

type Context struct {
	logger *logrus.Logger
	svc    *service.Context
}

func New(l *logrus.Logger, svc *service.Context) (*Context, error) {
	return &Context{logger: l, svc: svc}, nil
}

// RegisterHandlers
func (h *Context) RegisterHandlers(app *fiber.App) {
	// swagger:operation GET /api/v1/trivia trivia GetTrivia
	// ---
	// operationId: GetTrivia
	// summary: This operation retrieves trivia from the database according to the parameters specified. If you specify no parameters, the API returns first 20 trivia.
	// parameters:
	// - name: Authorization
	//   in: header
	//   description: Basic oAuth
	//   required: true
	//   type: string
	// - name: Content-Type
	//   in: header
	//   description: The media type of the request entity. Set this to application/json.
	//   required: true
	//   type: string
	// - name: Accept-Language
	//   in: header
	//   description: Language and country code. Default is en-US.
	//   required: false
	//   type: string
	// - name: TransactionId
	//   in: header
	//   description: A unique identifier for the transaction, up to 25 characters. The following characters are allowed \n letters, numbers, hyphens (-), and underscores (_)./n Important- You must ensure this is a unique id.
	//   required: true
	//   type: string
	// - name: text
	//   in: query
	//   description: The text for which to return trivia.
	//   required: false
	//   type: string
	// - name: number
	//   in: query
	//   description: The number for which to return trivia.
	//   required: false
	//   type: string
	// - name: type
	//   in: query
	//   description: The type for which to return trivia.
	//   required: false
	//   type: string
	// - name: size
	//   in: query
	//   description: The number of transactions to return per page in the result set. Default is 20
	//   required: false
	//   type: string
	// - name: page
	//   in: query
	//   description: The index number of the page to return. Page index numbering starts at 0. Specifying page=0 returns the first page of the result set.
	//   required: false
	//   type: string
	// - name: found
	//   in: query
	//   description: Queries based on found boolean value.
	//   required: false
	//   type: bool
	// responses:
	//   '200':
	//     description: response contains list of trivia objects.
	//     schema:
	//       $ref: '#/definitions/trivia'
	//   default:
	//     description: unexpected error
	//     schema:
	//       "$ref": "#/definitions/errorModel"
	app.Get("/api/v1/trivia", h.GetTrivia)
}

func (h *Context) GetTrivia(c *fiber.Ctx) error {

	// transaction id will be saved in each logs
	trxId := c.GetReqHeaders()["TransactionId"]

	h.logger.Infof("[%s]: GetTrivia api started", trxId)
	queryStr := string(c.Request().URI().QueryString())
	qp, err := url.ParseQuery(queryStr)

	if err != nil {
		h.logger.Errorf("[%s]: url.ParseQuery failed with %v", trxId, qp)
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "provided query params are incorrect",
		})
	}

	// overriding customQueryParser for number to handle + sign
	customQp := customQueryParser(queryStr)
	if val := customQp.Get("number"); val != "" {
		qp.Set("number", val)
	}

	// validate query params and convert to map
	err = validateQueryParams(qp)
	if err != nil {
		h.logger.Errorf("[%s]: filter and sort fields are not valid: %v", trxId, err)
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": err,
		})
	}

	fmt.Println(qp)
	res, err := h.svc.GetTrivia(trxId, qp)
	if err != nil {
		h.logger.Errorf("[%s]: error while fetching trivias: ", trxId, err)
		c.Status(fiber.ErrInternalServerError.Code)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	h.logger.Infof("[%s]: Get trivia api successfully completed.", trxId)
	c.Status(fiber.StatusOK)
	return c.JSON(res)
}

func validateQueryParams(qp url.Values) error {

	for q, val := range qp {
		if q == common.QPSORT {
			keys := strings.Split(val[0], ",")
			if len(keys) == 2 {
				sortField := strings.TrimSpace(keys[0]) // extract sort field
				if len(strings.TrimSpace(sortField)) > 0 {
					if util.FindString(common.AllowedSortFields, strings.TrimSpace(sortField)) < 0 { // check for sort field is valid
						return fmt.Errorf("passed sort field param %s is not allowed for filtering trivia", sortField)
					}
				}
			}
			continue
		}
		if util.FindString(common.AllowedQPs, q) < 0 {
			return fmt.Errorf("passed query param %s is not allowed for filtering trivia", q)
		}
	}
	return nil
}

func customQueryParser(query string) url.Values {

	m := make(url.Values)
	for query != "" {
		key := query
		if i := strings.IndexAny(key, "&;"); i >= 0 {
			key, query = key[:i], key[i+1:]
		} else {
			query = ""
		}
		if key == "" {
			continue
		}
		value := ""
		if i := strings.Index(key, "="); i >= 0 {
			key, value = key[:i], key[i+1:]
		}
		m[key] = append(m[key], value)
	}
	return m
}

package common

import "go.mongodb.org/mongo-driver/mongo"

var (
	AllowedQPs        = []string{"text", "number", "type", "found"}
	AllowedSortFields = []string{"text", "number", "type", "found"}
)

const (
	//db related constant
	DataTypeString    = "string"    //For all string params
	DataTypeNumber    = "number"    //For all int, float params
	DataTypeBool      = "bool"      //For Boolean params
	DataTypeDate      = "date"      //For DateTime filter fields
	DataTypeInterface = "interface" //For DateTime filter fields

	SearchTypeEXACT     = "EXACT"   // Can be used for all data types
	SearchTypePATTERN   = "PATTERN" // can be used for strings
	SearchTypeRANGE     = "RANGE"   // can be used for date and numbers only
	SearchTypeNOT_EQUAL = "NE"
	SearchTypeIN        = "IN" // used for "IN" Query

	QPSORT = "sort"
)

type FilterField struct {
	BsonName   string
	StructName string
	Datatype   string //Refer constants for proper datatype string
}

type FilterFieldValue struct {
	BsonName    string
	SearchValue string   //Value to be searched in DB
	StartValue  string   // To be used with SearchTypeRANGE. Send this value if doing a greater than x search
	EndValue    string   // To be used with SearchTypeRANGE. Send this value if doing a less than y search
	SearchType  string   // EXACT, PATTERN, RANGE
	SearchList  []string // To be used with SearchTypeIN
}

//CollRule is a type that holds a rules or metadata of collection.
type CollRule struct {
	ParentDB      *mongo.Database //a handle to the database
	Name          string          //collection name
	DefaultID     FilterField
	AllowedFilter []FilterField
}

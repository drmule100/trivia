package middleware

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/gofiber/fiber/v2/middleware/basicauth"
)

func BasicAuthConfig() (basicauth.Config, error) {

	// read the users and password
	pwd, err := os.Getwd()
	if err != nil {
		return basicauth.Config{}, err
	}
	folder := pwd + string(os.PathSeparator) + "internal" + string(os.PathSeparator) + "json" + string(os.PathSeparator)

	content, err := ioutil.ReadFile(folder + "users.json")
	if err != nil {
		return basicauth.Config{}, err
	}
	var userPwd map[string]string

	err = json.Unmarshal(content, &userPwd)
	if err != nil {
		return basicauth.Config{}, err
	}
	return basicauth.Config{
		Users: userPwd,
		Realm: "Forbidden",
		/* Authorizer: func(user, pass string) bool {
			// custom authorization
		}, */
		//Unauthorized:    unauthorizeHandler,
		ContextUsername: "_user",
		ContextPassword: "_pass",
	}, nil
}

package middleware

import "github.com/gofiber/fiber/v2/middleware/monitor"

func MonitorCfg() monitor.Config {
	return monitor.Config{
		APIOnly: false,
		Next:    nil,
	}
}

package util

func FindString(arr []string, key string) int {
	for i, val := range arr {
		if val == key {
			return i
		}
	}
	return -1
}

package service

import (
	"net/url"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/drmule100/trivia/internal/common"
	"gitlab.com/drmule100/trivia/internal/model"
	trivia "gitlab.com/drmule100/trivia/pkg"
)

type Context struct {
	logger *logrus.Logger
	dbCtx  *model.Context
}

func New(l *logrus.Logger, model *model.Context) (*Context, error) {
	return &Context{logger: l, dbCtx: model}, nil
}

// GetTrivia
func (svc *Context) GetTrivia(trxId string, qps url.Values) ([]trivia.Trivia, error) {

	var page, limitCount int
	var sortBy map[string]int
	// create filter and sort fields
	var ffvs = make([]common.FilterFieldValue, 0)
	for k, qp := range qps {
		switch k {
		case "page":
			page, _ = strconv.Atoi(qp[0])
		case "size":
			limitCount, _ = strconv.Atoi(qp[0])
		case "text":
			text := qp[0]
			var ffv = common.FilterFieldValue{BsonName: "text", SearchType: common.SearchTypeEXACT, SearchValue: text}
			ffvs = append(ffvs, ffv)
		case "number":
			number := qp[0]
			var ffv = common.FilterFieldValue{BsonName: "number", SearchType: common.SearchTypeEXACT, SearchValue: number}
			ffvs = append(ffvs, ffv)
		case "found":
			if strings.ToLower(qp[0]) == "true" {
				var ffvRefundStatus = common.FilterFieldValue{BsonName: "found", SearchType: common.SearchTypeEXACT, SearchValue: "true"}
				ffvs = append(ffvs, ffvRefundStatus)
			} else {
				var ffvRefundStatus = common.FilterFieldValue{BsonName: "found", SearchType: common.SearchTypeEXACT, SearchValue: "false"}
				ffvs = append(ffvs, ffvRefundStatus)
			}
		case "type":
			triviaType := qp[0]
			var ffv = common.FilterFieldValue{BsonName: "type", SearchType: common.SearchTypeEXACT, SearchValue: triviaType}
			ffvs = append(ffvs, ffv)
		case "sort":
			keys := strings.Split(qp[0], ",")
			if len(keys) == 2 {
				var order = -1
				sortOrder := strings.ToUpper(strings.TrimSpace(keys[1]))
				if len(sortOrder) != 0 && strings.EqualFold(sortOrder, "ASC") {
					order = 1
				}
				sortBy = map[string]int{strings.TrimSpace(keys[0]): order}
			}
			break
		default:
			//vars[k] = qp[0]
		}
	}

	if page <= 0 {
		page = 1
	}
	if limitCount <= 0 {
		limitCount = 20
	}
	skipCount := (page - 1) * limitCount

	var resp []trivia.Trivia
	err := svc.dbCtx.FetchDocuments(ffvs, sortBy, int64(skipCount), int64(limitCount), nil, &resp, trxId)
	if err != nil {
		svc.logger.Errorf("[%s]: error while fetchDocuments: %v", trxId, err)
		return resp, err
	}
	return resp, nil
}

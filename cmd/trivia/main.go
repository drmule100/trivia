// Trivia API
//
// This is a description of Trivia APIs.
//
//     Schemes: http, https
//     Version: 0.1.0
//     basePath: http://127.0.0.1:3000
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package main

import (
	"context"
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
	fiberLogger "github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	log "github.com/sirupsen/logrus"
	"gitlab.com/drmule100/trivia/internal/handlers"
	"gitlab.com/drmule100/trivia/internal/middleware"
	"gitlab.com/drmule100/trivia/internal/model"
	"gitlab.com/drmule100/trivia/internal/service"
)

const (
	defaultDBName = "main"
)

func main() {

	fmt.Println("starting server...")
	// initialize logger
	// open log file with write/append permissions
	var logger = log.New()
	logFile, err := os.OpenFile("./service.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		logger.Out = logFile
	} else {
		logger.Info("Failed to log to file, using default stderr")
	}
	defer logFile.Close()
	logger.Info("starting server...")

	app := fiber.New()
	// setting up middlewares

	// initializing logger for middleware, which will only log request url, method, response time and status code
	file, err := os.OpenFile("./service_request.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		logger.Fatalf("error opening file: %v", err)
	}
	defer file.Close()

	app.Use(fiberLogger.New(fiberLogger.Config{
		Output: file,
	}))

	// using basic auth for authorization

	basicAuthCfg, err := middleware.BasicAuthConfig()
	if err != nil {
		logger.Infof("error generating basic auth config: %v using default basic auth", err)
		app.Use(basicauth.New(basicauth.ConfigDefault))
	} else {
		app.Use(basicauth.New(basicAuthCfg))
	}

	//monitoring middleware
	app.Get("/dashboard", monitor.New(middleware.MonitorCfg()))

	// connecting to database
	logger.Info("connecting to db ...")
	mgo, db, err := model.Connect(defaultDBName)
	if err != nil {
		logger.Fatal("error while connecting db: ", err)
	}
	defer model.Disconnect(mgo)

	dbCtx, err := model.New(logger, db)
	if err != nil {
		logger.Fatal("error while initializing db context")
	}

	ctx := context.Background()
	// load documents to db
	dbCtx.LoadDocuments(ctx)

	triviaSvc, err := service.New(logger, dbCtx)
	if err != nil {
		log.Fatal("error while initializing trivia service context")
	}

	handler, err := handlers.New(logger, triviaSvc)
	if err != nil {
		logger.Fatal("error while initializing handler context")
	}

	// register api handler
	handler.RegisterHandlers(app)

	logger.Info("server started...")
	app.Listen(":3000")
}

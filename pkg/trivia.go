package trivia

import "go.mongodb.org/mongo-driver/bson/primitive"

// swagger:model trivia
type Trivia struct {
	ID primitive.ObjectID `json:"-" bson:"_id,omitempty" yaml:"-"`
	// Text is description of trivia
	Text string `json:"text,omitempty" bson:"text,omitempty" yaml:"text,omitempty"`
	// number is magical number of trivia
	Number interface{} `json:"number" bson:"number" yaml:"number"`
	// found is whether trivia is found or not
	Found bool `json:"found,omitempty" bson:"found,omitempty" yaml:"found,omitempty"`
	// type of object, i.e trivia
	Type string `json:"type,omitempty" bson:"type,omitempty" yaml:"type,omitempty"`
}

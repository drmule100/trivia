module gitlab.com/drmule100/trivia

go 1.16

require (
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gofiber/fiber/v2 v2.28.0
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tidwall/pretty v1.1.0 // indirect
	go.mongodb.org/mongo-driver v1.8.4
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
